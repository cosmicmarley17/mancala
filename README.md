# Mancala Console

![A command-line interface for a game of Mancala. There's a text-art banner at the top and a depiction of a Mancala board, followed by a prompt to make a move.](docs/images/mancala_console_screenshot.webp "The user-interface of Mancala Console")

## About

A virtual mancala board game in the terminal, with hot-seat local multiplayer.

## Modular structure

This project uses my library [libmancala](https://codeberg.org/cosmicmarley17/libmancala) to provide the underlying game logic.

The library is frontend-agnostic, so other Mancala video games can use libmancala to implement their game mechanics.

