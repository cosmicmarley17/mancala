use libmancala::{MancalaBoard, Move, MoveResult, Player}; // game logic backend
use std::io; // for I/O
use std::io::Write; // for flushing stdout

fn main() {
    let mut board = MancalaBoard::new();
    let mut game_state: MoveResult;
    loop {
        let current_player = board.turn.to_owned();
        draw_board(&board, &current_player); // paint the TUI

        // gets user input for which pit to move, and retries if an error occurs
        let move_input: Move = loop {
            print!("Choose a pit to move: ");
            io::stdout().flush().expect("ERROR: Failed to flush stdout"); // flush stdout so input is on same line
            let mut input = String::new(); // creates input variable

            //read command-line input
            io::stdin()
                .read_line(&mut input) // reads input
                .expect("ERROR: Failed to read line.");

            // match and compare input to convert to Move (trim newline)
            // assigns to Some(Move) if valid input is received, otherwise returns None
            let move_input = match input.as_str().trim() {
                "A" | "a" | "1" => Some(Move::A),
                "B" | "b" | "2" => Some(Move::B),
                "C" | "c" | "3" => Some(Move::C),
                "D" | "d" | "4" => Some(Move::D),
                "E" | "e" | "5" => Some(Move::E),
                "F" | "f" | "6" => Some(Move::F),
                _ => None,
            };
            if move_input.is_some() {
                break move_input.unwrap();
            } else {
                draw_board(&board, &current_player);
                println!("Invalid input! Enter A,B,C,D,E, or F.\n");
            }
        };

        let bonus_turn: bool; // whether a bonus turn is earned
        let captured: Option<u32>; // amount of "captured" pieces, if any
        (game_state, bonus_turn, captured) = board.update(&move_input);
        draw_board(&board, &current_player); // update the TUI

        match captured {
            Some(x) => println!("You captured {} pieces!", x),
            None => (),
        }

        match game_state {
            MoveResult::Won(Player::P1) => {
                println!("Player 1 won!");
                break;
            }
            MoveResult::Won(Player::P2) => {
                println!("Player 2 won!");
                break;
            }
            MoveResult::Draw => {
                println!("Game over: draw!");
                break;
            }
            MoveResult::Invalid => {
                println!("Invalid move! Press ENTER to try again. ");
                io::stdout().flush().expect("ERROR: Failed to flush stdout"); // flush stdout so input is on same line
                let _ = io::stdin().read_line(&mut String::from("")).unwrap();
            }
            MoveResult::Continuing => {
                if bonus_turn {
                    print!("BONUS TURN! Press ENTER to make another move!");
                } else {
                    print!("Press ENTER to end your turn. ");
                }
                io::stdout().flush().expect("ERROR: Failed to flush stdout"); // flush stdout so input is on same line
                let _ = io::stdin().read_line(&mut String::from("")).unwrap();
            }
        }
    }
}

// paints the board in TUI
// board: the MancalaBoard to display
// perspective: from which player's visual perspective the board is shown
fn draw_board(board: &MancalaBoard, perspective: &Player) {
    // map components of board to variables to correctly depict player perspective
    let row_near;
    let row_far;
    let store_right;
    let store_left;
    let player_name; // string representation of the current player
    match perspective {
        Player::P1 => {
            row_near = board.p1_board;
            row_far = board.p2_board;
            store_right = board.p1_store;
            store_left = board.p2_store;
            player_name = String::from("Player One");
        }
        Player::P2 => {
            row_near = board.p2_board;
            row_far = board.p1_board;
            store_right = board.p2_store;
            store_left = board.p1_store;
            player_name = String::from("Player Two");
        }
    }

    clearscreen::clear().unwrap(); // clear screen

    println!("/--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--\\");
    println!("| . . . . . .  M  A  N  C  A  L  A  . . . . . . |");
    println!("\\--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--/");
    println!();
    println!("{}'s turn:", player_name);
    println!();

    //print opposite player's side (in reverse because it's the opposite side of the board)
    print!("[    ] ");
    for i in row_far.iter().rev() {
        let pit = i;
        let pit = pad_number(*pit);
        print!("({}) ", pit)
    }
    print!("[    ] \n");

    //print middle gap + stores
    let store_left = pad_number(store_left);
    let store_right = pad_number(store_right);
    println!(
        "[ {} ]                               [ {} ]",
        store_left, store_right
    );

    //print current player's side
    print!("[    ] ");
    for i in row_near {
        let pit = i;
        let pit = pad_number(pit);
        print!("({}) ", pit)
    }
    print!("[    ] \n");

    //print bottom (board labels)
    println!("         A    B    C    D    E    F   STORE");
    println!();
}

// converts a number to a string and
// adds a space to the beginning if the number is a single digit.
fn pad_number(num: u32) -> String {
    if num > 9 {
        return num.to_string();
    } else {
        return " ".to_string() + &num.to_string();
    }
}
